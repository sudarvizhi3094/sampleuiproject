////
////  TableViewExtension.swift
////  sampleProject
////
////  Created by Sudar on 14/01/20.
////  Copyright © 2020 Sudar. All rights reserved.
////
//
//import UIKit
//import Alamofire
//import ObjectMapper
//import AlamofireObjectMapper
//
//struct MultipartData {
//    var data: Data?
//    var valueKey: String?
//    var dataName: String
//}
//
//class WebServices: UIViewController {
//    
//    static let shared = WebServices()
//    
//    func requestToApi<T: Mappable>(type: [T].Type,
//                                   with endPointURL: String,
//                                   urlMethod: HTTPMethod!,
//                                   showLoader: Bool,
//                                   params: Parameters? = nil,
//                                   accessTokenAdd: Bool? = true,
//                                   failureReturen: Bool? = false,
//                                   encode: ParameterEncoding? = JSONEncoding.default,                                   completion: @escaping(_ result: [DataResponse<T>]?) -> Void) {
//        
//        
//        guard NetworkState.isConnected() else {
//            self.showErrorMessage(message: Constants.noNetwork)
//            return
//        }
//        
//       
//        
//        //Form base url & add header (if both same )
//        let url = endPointURL
//        
//        let headers = [Constants.RequestType: Constants.RequestValue,
//                       Constants.ContentType: Constants.ContentValue]
//        
//       
//        
//        print("Request URL: \(url)")
//        print("Parameters: \(params ?? [:])")
//        
//        
//        //Alamofire request
//        Alamofire.request(url, method: urlMethod!, parameters: params, encoding: encode!, headers: headers).validate().responseObject { (response: DataResponse<T>) in
//           
//            
//            //Print response
//            //print("Response:--->",response.result.value as Any)
//            print("localizedDescription:--->",response.error?.localizedDescription as Any)
//            print("error:--->",response.error as Any)
//            
//            
//            //Response validate
//            switch response.result {
//            case .success:
//                completion(response)
//            case .failure:
//                if failureReturen! {
//                    completion(response)
//                }
//                else if let alamoError = response.result.error, let err = alamoError as? URLError {
//                    print("alamoError")
//                    self.errorCode(errorCode: err)
//                }
//                else if let data = response.data {
//                    print("alamoError data")
//                    self.showErrorMessage(responseData: data)
//                }
//                else {
//                    print("alamoError else")
//                    self.showErrorMessage(message: response.error.debugDescription)
//                }
//            }
//        }
//    }
//    
//    func requestParamWithData<T: Mappable>(type: T.Type,with endPointURL: String,
//                                           multipartData: MultipartData?,
//                                           showLoader: Bool,
//                                           params: Parameters? = nil,
//                                           accessTokenAdd: Bool? = true,
//                                           failureReturen: Bool? = false,
//                                           encode: ParameterEncoding? = JSONEncoding.default,                              completion: @escaping(_ result: DataResponse<T>?) -> Void) {
//        
//        //Network reachable check
//        guard NetworkState.isConnected() else {
//            self.showErrorMessage(message: Constants.noNetwork)
//            return
//        }
//        
//      
//        //Form base url & add header (if both same )
//        let url = endPointURL
//    
//        
//        let headers = [Constants.RequestType: Constants.RequestValue,
//                       Constants.ContentType: Constants.MultiPartValue]
//        
//        
//        print("Request URL: \(url)")
//        print("Parameters: \(params ?? [:]))")
//        Alamofire.upload(multipartFormData: { (multipartFormData) in
//            for (key, value) in params ?? [:]{
//                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
//            }
//            
//            guard let mData = multipartData else {
//                return
//            }
//            multipartFormData.append(mData.data!, withName: mData.valueKey!, fileName: "\(mData.dataName).jpeg", mimeType: "image/jpeg")
//        }, to:url)
//        { (result) in
//            switch result {
//            case .success(let upload, _, _):
//                
//                upload.uploadProgress(closure: { (Progress) in
//                    print("Upload Progress: \(Progress.fractionCompleted)")
//                })
//                upload.responseObject(completionHandler: { (response) in
//                    
//                    completion(response)
//                })
//            case .failure(let error):
//                print(error)
//            }
//            
//        }
//    }
//    
//    
//    func requestToImageUpload<T: Mappable>(type: T.Type,with endPointURL: String,
//                                           imageData: [String:Data]?,
//                                           showLoader: Bool,
//                                           params: Parameters? = nil,
//                                           accessTokenAdd: Bool? = true,
//                                           failureReturen: Bool? = false,
//                                           encode: ParameterEncoding? = JSONEncoding.default,                              completion: @escaping(_ result: DataResponse<T>?) -> Void) {
//        
//        //Network reachable check
//        guard NetworkState.isConnected() else {
//            self.showErrorMessage(message: Constants.noNetwork)
//            return
//        }
//        
//        
//       
//        //Form base url & add header (if both same )
//        let url = endPointURL
//        
//        let headers: HTTPHeaders = [Constants.RequestType: Constants.RequestValue,
//                                    Constants.ContentType: Constants.multiPartValue]
//        
//      
//        
//        print("Request URL: \(url)")
//        print("Parameters: \(params ?? [:]))")
//        Alamofire.upload(multipartFormData: { (multipartFormData) in
//            
//            //Param Mapping
//            for (key, value) in params ?? [:] {
//                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
//            }
//            
//            //File Name
//            let uniqueString: String = ProcessInfo.processInfo.globallyUniqueString
//            
//            //Data Mapping
//            for (key,value) in imageData ?? [:] {
//                multipartFormData.append(value, withName: key, fileName: uniqueString+".png", mimeType: "image/png")
//            }
//            
//        }, to: url, method: .post, headers: headers) { (result) in
//            
//            switch result {
//            case .success(let upload, _,_ ):
//                upload.uploadProgress(closure: { (progress) in
//                    print("Upload Progress: \(progress.fractionCompleted)")
//                })
//                upload.responseObject(completionHandler: { (response: DataResponse<T>) in
//                    
//                   
//                    
//                    if response.response?.statusCode == 200 {
//                        completion(response)
//                    }
//                    else if failureReturen! {
//                        completion(response)
//                    }
//                    else if let alamoError = response.result.error, let err = alamoError as? URLError {
//                        print("alamoError")
//                        self.errorCode(errorCode: err)
//                    }
//                    else if let data = response.data {
//                        print("alamoError data")
//                        self.showErrorMessage(responseData: data)
//                    }
//                    else {
//                        print("alamoError else")
//                        self.showErrorMessage(message: response.error.debugDescription)
//                    }
//                })
//            case .failure(let error):
//                self.showErrorMessage(message: error.localizedDescription)
//               
//            }
//        }
//    }
//    
//    func errorCode(errorCode: URLError) {
//        switch errorCode.code {
//        case .notConnectedToInternet:
////            if let topViewController = UIApplication.topViewController() {
////                AppAlert.shared.simpleAlert(view: topViewController, title: String.empty, message: Constants.noNetwork.localized)
////            }
//            break
//        case .timedOut:
////            if let topViewController = UIApplication.topViewController() {
////            }
//            break
//        case .networkConnectionLost:
////            if let topViewController = UIApplication.topViewController() {
////            }
//            break
//        default:
//            break
//        }
//    }
//    
//    func showErrorMessage(responseData: Data) {
//
//    }
//    
//    //Show error message
//    func showErrorMessage(message: String) {
//        
//        
//        /*if let topViewController = UIApplication.topViewController() {
//         AppAlert.shared.simpleAlert(view: topViewController, title: String.empty, message: message)
//         }*/
//    }
//    
//    // Method to convert JSON String to Dictionary
//    func stringToDictionary(text: String) -> [String: Any]? {
//        if let data = text.data(using: .utf8) {
//            do {
//                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
//            } catch {
//                print(error.localizedDescription)
//            }
//        }
//        return nil
//    }
//}
//
////MARK: - NetworkState
//
//class NetworkState {
//    class func isConnected() ->Bool {
//        return NetworkReachabilityManager()!.isReachable
//    }
//}
