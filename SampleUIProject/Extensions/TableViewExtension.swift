//
//  TableViewExtension.swift
//  sampleProject
//
//  Created by Sudar on 14/01/20.
//  Copyright © 2020 Sudar. All rights reserved.
//

import UIKit

extension UITableView {
    
    func register(nibName: String) {
        let Nib = UINib(nibName: nibName, bundle: nil)
        register(Nib, forCellReuseIdentifier: nibName)
    }
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}
