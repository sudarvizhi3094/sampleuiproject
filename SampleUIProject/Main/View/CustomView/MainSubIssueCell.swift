//
//  MainSubIssueCell.swift
//  sampleProject
//
//  Created by Sudar on 14/01/20.
//  Copyright © 2020 Sudar. All rights reserved.
//

import UIKit

class MainSubIssueCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var errorImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialLoads()
        
    }
    
    private func initialLoads(){
        setColors()
        setImage()
    }
    
    //Colors
    private func setColors(){
        self.contentView.backgroundColor = .groupTableViewBackground
        subTitleLabel.textColor = .lightGray

    }
    //Fonts
    private func setImage(){
        errorImage.image = UIImage(named: ImageString.error)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
