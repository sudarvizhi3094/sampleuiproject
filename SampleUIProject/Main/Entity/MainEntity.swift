//
//  MainEntity.swift
//  sampleProject
//
//  Created by Sudar on 14/01/20.
//  Copyright © 2020 Sudar. All rights reserved.
//

import Foundation
import ObjectMapper

struct MainDataEntity : Mappable {
//    var url : String?
//    var repository_url : String?
//    var labels_url : String?
//    var comments_url : String?
//    var events_url : String?
//    var html_url : String?
    var id : Int64?
//    var node_id : String?
    var number : Int32?
    var title : String?
    var user : User?
//    var labels : [String]?
    var state : String?
//    var locked : Bool?
//    var assignee : String?
//    var assignees : [String]?
//    var milestone : String?
//    var comments : Int?
    var created_at : String?
//    var updated_at : String?
//    var closed_at : String?
//    var author_association : String?
    var body : String?
    
    init() {
        
    }
    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

//        url <- map["url"]
//        repository_url <- map["repository_url"]
//        labels_url <- map["labels_url"]
//        comments_url <- map["comments_url"]
//        events_url <- map["events_url"]
//        html_url <- map["html_url"]
        id <- map["id"]
//        node_id <- map["node_id"]
        number <- map["number"]
        title <- map["title"]
        user <- map["user"]
//        labels <- map["labels"]
        state <- map["state"]
//        locked <- map["locked"]
//        assignee <- map["assignee"]
//        assignees <- map["assignees"]
//        milestone <- map["milestone"]
//        comments <- map["comments"]
        created_at <- map["created_at"]
//        updated_at <- map["updated_at"]
//        closed_at <- map["closed_at"]
//        author_association <- map["author_association"]
        body <- map["body"]
    }

}


struct User : Mappable {
    var login : String?
    var id : Int?
    var node_id : String?
    var avatar_url : String?
    var gravatar_id : String?
    var url : String?
    var html_url : String?
    var followers_url : String?
    var following_url : String?
    var gists_url : String?
    var starred_url : String?
    var subscriptions_url : String?
    var organizations_url : String?
    var repos_url : String?
    var events_url : String?
    var received_events_url : String?
    var type : String?
    var site_admin : Bool?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        login <- map["login"]
        id <- map["id"]
        node_id <- map["node_id"]
        avatar_url <- map["avatar_url"]
        gravatar_id <- map["gravatar_id"]
        url <- map["url"]
        html_url <- map["html_url"]
        followers_url <- map["followers_url"]
        following_url <- map["following_url"]
        gists_url <- map["gists_url"]
        starred_url <- map["starred_url"]
        subscriptions_url <- map["subscriptions_url"]
        organizations_url <- map["organizations_url"]
        repos_url <- map["repos_url"]
        events_url <- map["events_url"]
        received_events_url <- map["received_events_url"]
        type <- map["type"]
        site_admin <- map["site_admin"]
    }

}
