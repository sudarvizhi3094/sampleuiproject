#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif


FOUNDATION_EXPORT double Pods_SampleUIProject_SampleUIProjectUITestsVersionNumber;
FOUNDATION_EXPORT const unsigned char Pods_SampleUIProject_SampleUIProjectUITestsVersionString[];

