//
//  MainEntity+CoreDataProperties.swift
//  
//
//  Created by Sudar on 15/01/20.
//
//

import Foundation
import CoreData


extension MainEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MainEntity> {
        return NSFetchRequest<MainEntity>(entityName: "MainEntity")
    }

    @NSManaged public var body: String?
    @NSManaged public var created_at: String?
    @NSManaged public var id: Int64
    @NSManaged public var number: Int32
    @NSManaged public var state: String?
    @NSManaged public var title: String?
    @NSManaged public var login: String?
}
